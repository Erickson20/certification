'use strict'

const doc = require('../models/certification')
const category = require('../models/category');
const db = require('../models/mssql');

function Create(req, res) {
  
    doc.create({
        categoria: req.body.categoria,
        nombre: req.body.nombre,
        nmcategoria: req.body.nmcategoria,
        html: req.body.html,
        proceso: req.body.proceso
    }).then((result) => {
        res.send({documentos: result})
    }).catch((err) => {
        res.send({error: err})
    });
    
}

function AllCategory(req, res) {
    db.mssql.query("SELECT IDCATEGORY 'id', NMCATEGORY 'Nombre' FROM DCCATEGORY WHERE IDCATEGORY LIKE '%"+ req.params.id+"%'", { type: db.mssql.QueryTypes.SELECT})
  .then(category => {
    res.send({categoria: category})
  })
}

function EntidadSolServ(req, res) {
    db.mssql.query("SELECT TOP  1 *  FROM dyn"+req.params.id+"SolServ ", { type: db.mssql.QueryTypes.SELECT})
  .then(solicitud => {
    res.send({SolServ: solicitud})
  })
}


function AllCert(req, res) {

    doc.findAll(
        {
            where: {
                proceso: req.params.id
              }
        }
    )
    .then((result) => {
        res.send({documentos: result})
    })
    .catch((err) => {
        res.send({error: err})
    });
}


function OneCert(req, res) {
   
}



function CreateCertification(req, res ) {
    no_solicitud = "";
    institucion = "";

    

    
}

function Delete(req, res) {
    
}

function Update(req, res){
    doc.update({
        categoria: req.body.categoria,
        html: req.body.html,
        proceso: req.body.proceso
    },
    {
        where: {
            id: req.params.id
        }
    })
    .then((result) => {
        res.send({documentos: result})
    })
    .catch((err) => {
        res.send({error: err})
    });

}

module.exports = {
    Update,
    OneCert,
    AllCert,
    Create,
    Delete,
    AllCategory,
    EntidadSolServ,
    CreateCertification
}