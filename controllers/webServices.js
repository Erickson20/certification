
'use strict'
var soap = require('soap');
var args = {name: 'value'};

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"


function newDocument(params) {
    var soapHeaders = {
        'wsa:Action': 'http://tempuri.org/MyPortName/MyAction',
        'wsa:To': 'http://myservice.com/MyService.svc'
    };
    var auth = "Basic " + new Buffer("splws01" + ":" + "splws01").toString("base64");
    var url = 'https://se.servicios.gob.do/se/ws/dc_ws.php?wsdl';
    soap.createClient(url,{wsdl_options: {rejectUnauthorized: false}, wsdl_headers: {Authorization: auth}}, function(err, client) {
        //client.setSecurity(new soap.BasicAuthSecurity('splws01','splws01'))
        client.newDocument(params,  function(err, result) {
            console.log(result)
        });
        
    });
}




module.exports = {
newDocument
}