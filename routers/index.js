'use strict'
const express = require('express');
const doc = require('../controllers/certification');
const api = express.Router();



api.get('/documentos/:id', doc.AllCert);
api.get('/documentos/', doc.OneCert);
api.get('/category/:id', doc.AllCategory);
api.get('/SolServ/:id', doc.EntidadSolServ);
api.post('/documentos/', doc.Create);
api.put('/documentos/:id', doc.Update);




module.exports = api;