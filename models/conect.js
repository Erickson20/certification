'use strict'
const Sequelize = require('sequelize');
const config = require('../config');

const sql = new Sequelize(config.db, config.userDb, config.userPass, {
    host: config.host,
    dialect: config.dbType,
  
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
  
    
  });


  module.exports = {sql,Sequelize};