'use strict'
const db = require('./conect');



const Certification = db.sql.define('documentos', {
    id: { type: db.Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    nombre:{type: db.Sequelize.STRING},
    categoria: {type: db.Sequelize.STRING},
    nmcategoria: {type: db.Sequelize.STRING},
    html: {type: db.Sequelize.STRING},
    proceso: {type: db.Sequelize.STRING}
  });


module.exports = Certification;