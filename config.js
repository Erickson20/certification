module.exports ={
    port: process.env.PORT || 4000,
    host: process.env.HOST || 'localhost',
    dbType: process.env.Type || 'mysql',
    db: process.env.MYSQL || 'certificaciones',
    userDb: process.env.USER || 'root',
    userPass: process.env.PASS || '',
    SECRET_TOKEN: 'palolazo'
}