'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const hbs = require('express-handlebars');
const app = express();
const api = require('./routers/index')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.engine('.hbs', hbs({
    defaultLayout: 'default',
    extname: '.hbs'
}))
app.set('view engine', '.hbs')

app.use('/api', api)

app.get('/newCert/:id', (req, res) =>{
    res.render('newCert', {
        id: req.params.id
      });
})

app.get('/allCert/:proceso/:institucion', (req, res) =>{
    res.render('allCert', {
         proceso: req.params.proceso,
         institucion: req.params.institucion
      });
})
app.get('/answer/:institucion', (req, res) =>{
    res.render('answer', {
        institucion:  req.params.institucion
      });
})


module.exports = app;